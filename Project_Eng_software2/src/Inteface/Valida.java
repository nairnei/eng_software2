/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inteface;

import java.awt.event.KeyEvent;

/**
 *
 * @author ronai
 */
public interface Valida {
    
    public void validarEspecial(KeyEvent evt);
    
    public void validarMuitoEspecial(KeyEvent evt);
    
    public void validarInt(KeyEvent evt);
    
    public void validarChar(KeyEvent evt);
    
}
