package RN;

import IU.Principal;
import Inteface.Valida;
import Pers.Pers;
import VO.Livro;
import VO.Usuario;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Regra implements Valida
  {

    public ArrayList<Livro> estante;
    public ArrayList<Livro> filtro;
    public ArrayList<Usuario> usuarios;
    public int user;

    public Regra()
      {
      }

    public void popularTabela()
      {
        Pers p = new Pers();
      }

    public void PopularTabela_wait_E(ArrayList<Livro> estante, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.popularTabelaEstante_E(estante, u);
      }

    public void PopularTabela_wait_C(ArrayList<Livro> estante, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.popularTabelaEstante_C(estante, u);
      }

    public void PopularTabela_wait_P(ArrayList<Livro> estante, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.popularTabelaEstante_P(estante, u);
      }

    public void PopularTabela_wait2(ArrayList<Livro> filtro, String text, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.popularTabelaEstanteComFiltro(filtro, text, u);
      }

    public void Busca_Compra_Filtro(ArrayList<Livro> filtro, String text, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.popularTabelaCompraComFiltro(filtro, text, u);
      }

    public void Busca_Publicacao_Filtro(ArrayList<Livro> filtro, String text, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.popularTabelaPublicacao(filtro, text, u);
      }

    public void comprar(int id, double valor, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.comprar(id, valor, u);
        p.addNaEstante(id, u);
      }

    public void popularTabela_E(JTable jTable3, Usuario e) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        estante = new ArrayList();
        PopularTabela_wait_E(estante, e);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < estante.size(); i++)
        {
            tabela.addRow(obj);
            jTable3.setValueAt(estante.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(estante.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(estante.get(i).getData(), i, 2);
            jTable3.setValueAt(estante.get(i).getValor(), i, 3);
        }
      }

    public void popularTabela_C(JTable jTable3, Usuario u) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        estante = new ArrayList();
        PopularTabela_wait_C(estante, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < estante.size(); i++)
        {
            tabela.addRow(obj);
            jTable3.setValueAt(estante.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(estante.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(estante.get(i).getId_autor(), i, 2);
            jTable3.setValueAt(estante.get(i).getData(), i, 3);
            jTable3.setValueAt(estante.get(i).getValor(), i, 4);
        }
      }

    public void popularTabela_P(JTable jTable3, Usuario u) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        estante = new ArrayList();
        PopularTabela_wait_P(estante, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < estante.size(); i++)
        {
            tabela.addRow(obj);
            jTable3.setValueAt(estante.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(estante.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(estante.get(i).getValor(), i, 2);
            //capa.setIcon(estante.get(i).getIcon());
        }
      }

    public void Busca_Estante(JTable jTable3, String text, Usuario u) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        filtro = new ArrayList();
        PopularTabela_wait2(filtro, text, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < filtro.size(); i++)
        {
            tabela.addRow(obj);
            jTable3.setValueAt(filtro.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(filtro.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(filtro.get(i).getData(), i, 2);
            jTable3.setValueAt(filtro.get(i).getValor(), i, 3);
            //capa.setIcon(estante.get(i).getIcon());
        }
      }

    public void Busca_Compra(JTable jTable3, String text, Usuario u) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        filtro = new ArrayList();
        Busca_Compra_Filtro(filtro, text, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < filtro.size(); i++)
        {
            tabela.addRow(obj);
            jTable3.setValueAt(filtro.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(filtro.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(filtro.get(i).getId_autor(), i, 2);
            jTable3.setValueAt(filtro.get(i).getData(), i, 3);
            jTable3.setValueAt(filtro.get(i).getValor(), i, 4);
        }
      }

    public void popularTabela_U(JTable tabela_conf) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) tabela_conf.getModel();
        while (tabela_conf.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        usuarios = new ArrayList();
        popularTabela_U(usuarios);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < usuarios.size(); i++)
        {
            tabela.addRow(obj);
            tabela_conf.setValueAt(usuarios.get(i).getId(), i, 0);
            tabela_conf.setValueAt(usuarios.get(i).getNome(), i, 1);
            tabela_conf.setValueAt(usuarios.get(i).getSaldo(), i, 2);
        }
      }

    public void Busca_Publicacao(JTable jTable3, String text, Usuario u) throws SQLException
      {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0)
        {
            tabela.removeRow(0);
        }
        int i;
        filtro = new ArrayList();
        Busca_Publicacao_Filtro(filtro, text, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < filtro.size(); i++)
        {
            tabela.addRow(obj);
            jTable3.setValueAt(filtro.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(filtro.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(filtro.get(i).getValor(), i, 2);
        }
      }

    @Override
    public void validarEspecial(KeyEvent evt)
      {
        String c = "_@-0987654321.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if (!c.contains(evt.getKeyChar() + ""))
        {
            evt.consume();
        }
      }

    /**
     *
     * @param evt
     */
    @Override
    public void validarMuitoEspecial(KeyEvent evt)
      {
        String c = "_@-0987654321.abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!.:? ";
        if (!c.contains(evt.getKeyChar() + ""))
        {
            evt.consume();
        }
      }

    @Override
    public void validarInt(KeyEvent evt)
      {
        String c = "0987654321.";
        if (!c.contains(evt.getKeyChar() + ""))
        {
            evt.consume();
        }
      }

    @Override
    public void validarChar(KeyEvent evt)
      {
        String c = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
        if (!c.contains(evt.getKeyChar() + ""))
        {
            evt.consume();
        }
      }

    //sobrecarga
    public void inserirImagem(JLabel jLabel)
      {
        Pers p = new Pers();
        //  p.inserirImagem(jLabel);
      }

//sobrecarga
    public void inserirImagem(JLabel capa1, JLabel capa2, JLabel capa3, JLabel capa4, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.inserirImagem_Stand(capa1, capa2, capa3, capa4, u);
      }

    public void logar(Usuario user, int parseInt, Principal iu) throws SQLException
      {
        Pers p = new Pers();
        if (p.Logar(user, parseInt) == true)
        {
            iu.setVisible(true);
            iu.setUser(user);
        } else
        {
            JOptionPane.showMessageDialog(iu, "login ou senha invalidos");
        }
      }

    public void cadastrar(String text, int parseInt) throws SQLException
      {
        Pers p = new Pers();
        if (p.Logar(text) == false)
        {
            p.cadastrar(text, parseInt);
            JOptionPane.showMessageDialog(null, "cadastro realizado");
        } else
        {
            JOptionPane.showMessageDialog(null, "Login utilizado");
        }
      }

    public void search(String text) throws SQLException
      {
        Pers p = new Pers();
        p.search(text);
      }

    public void alterarSaldo(int text, Double parseDouble) throws SQLException
      {
        Pers p = new Pers();
        p.alterarSaldo(text, parseDouble);
      }

    public void enviar_comentario(int aux, String text, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.enviar_comentario(aux, text, u);
      }

    public void mostrarComentario(int aux) throws SQLException
      {
        Pers p = new Pers();
        p.mostrarComentario(aux);
      }

    public void depositar(int aux, Double valor) throws SQLException
      {
        Pers p = new Pers();
        p.alterarSaldo(aux, valor);
      }

    public void remover_ebook(int id_livro) throws SQLException
      {
        Pers p = new Pers();
        if (p.verifica_livro(id_livro) == true)
        {
            JOptionPane.showMessageDialog(null, "Você não pode remover esse livro! Ele foi comprado.");
        } else
        {
            p.remover(id_livro);
        }
      }

    public void ler(int i) throws SQLException, IOException
      {
        Pers p = new Pers();
        p.ler(i);
      }

    public void alterarSenha(JPasswordField jPasswordField1, int senha2, Usuario u) throws SQLException
      {
        Pers p = new Pers();
        p.alterarSenha(jPasswordField1, senha2, u);
      }

    private void popularTabela_U(ArrayList<Usuario> usuarios) throws SQLException
      {
        System.out.println("entrei no wait u");
        Pers p = new Pers();
        p.popularTabelaEstante_U(usuarios);
      }

  }
