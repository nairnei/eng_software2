/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RN;

import Pers.Pers;
import Pers.Pers_Consulta;
import Pers.Pers_Publicacao;
import VO.Livro;
import VO.Usuario;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author riochens
 */
public class RN_Publicacao {

    public ArrayList<Livro> estante;
    public ArrayList<Livro> filtro;
    public ArrayList<Usuario> usuarios;
    public int user;

    public RN_Publicacao() {
    }

    //----------------
    public void PopularTabela_wait_E(ArrayList<Livro> estante, Usuario u) {

        Pers_Consulta p = new Pers_Consulta();
        p.popularTabelaEstante_E(estante, u);

    }

    public void popularTabela_E(JTable jTable3, Usuario e) {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0) {
            tabela.removeRow(0);
        }
        int i;
        estante = new ArrayList();

        PopularTabela_wait_E(estante, e);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < estante.size(); i++) {
            tabela.addRow(obj);

            jTable3.setValueAt(estante.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(estante.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(estante.get(i).getData(), i, 2);
            jTable3.setValueAt(estante.get(i).getValor(), i, 3);

        }
    }

    //----------------
    public void PopularTabela_wait_C(ArrayList<Livro> estante, Usuario u) {
        Pers_Consulta p = new Pers_Consulta();
        p.popularTabelaEstante_C(estante, u);

    }

    public void popularTabela_C(JTable jTable3, Usuario u) {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0) {
            tabela.removeRow(0);
        }
        int i;
        estante = new ArrayList();

        PopularTabela_wait_C(estante, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < estante.size(); i++) {
            tabela.addRow(obj);
            jTable3.setValueAt(estante.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(estante.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(estante.get(i).getId_autor(), i, 2);
            jTable3.setValueAt(estante.get(i).getData(), i, 3);
            jTable3.setValueAt(estante.get(i).getValor(), i, 4);

        }
    }
    //----------------

    //----------------
    public void PopularTabela_wait_P(ArrayList<Livro> estante, Usuario u) {
        Pers_Consulta p = new Pers_Consulta();
        p.popularTabelaEstante_P(estante, u);

    }

    public void popularTabela_P(JTable jTable3, Usuario u) {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0) {
            tabela.removeRow(0);
        }
        int i;
        estante = new ArrayList();

        PopularTabela_wait_P(estante, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < estante.size(); i++) {
            tabela.addRow(obj);

            jTable3.setValueAt(estante.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(estante.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(estante.get(i).getValor(), i, 2);
            //capa.setIcon(estante.get(i).getIcon());

        }
    }
    //----------------

    //----------------
    public void remover_ebook(int id_livro) {
        Pers_Publicacao p = new Pers_Publicacao();
        if (p.verifica_livro(id_livro) == true) {
            JOptionPane.showMessageDialog(null, "Você não pode remover esse livro! Ele foi comprado.");
        } else {
            p.remover(id_livro);
        }

    }

    //----------------
    //----------------
    public void Busca_Publicacao_Filtro(ArrayList<Livro> filtro, String text, Usuario u) {
        Pers_Consulta p = new Pers_Consulta();
        p.popularTabelaPublicacao(filtro, text, u);

    }

    public void Busca_Publicacao(JTable jTable3, String text, Usuario u) {
        DefaultTableModel tabela = (DefaultTableModel) jTable3.getModel();
        while (jTable3.getRowCount() > 0) {
            tabela.removeRow(0);
        }
        int i;
        filtro = new ArrayList();

        Busca_Publicacao_Filtro(filtro, text, u);
        Object[] obj = new Object[tabela.getColumnCount()];
        for (i = 0; i < filtro.size(); i++) {
            tabela.addRow(obj);

            jTable3.setValueAt(filtro.get(i).getId_livro(), i, 0);
            jTable3.setValueAt(filtro.get(i).getTitulo_livro(), i, 1);
            jTable3.setValueAt(filtro.get(i).getValor(), i, 2);

        }
    }
    //----------------//----------------

    public void inserirImagem(JLabel capa1, JLabel capa2, JLabel capa3, JLabel capa4, Usuario u) {
        Pers_Consulta p = new Pers_Consulta();
        p.inserirImagem_Stand(capa1, capa2, capa3, capa4, u);

    }

}
