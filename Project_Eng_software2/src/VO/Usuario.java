/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;
import Abstract.User;

/**
 *
 * @author ronai
 */
public class Usuario extends User{
    
    private Double saldo;

    public Usuario(String nome) {
        this.nome = nome;
    }

    public Usuario(int id, String nome, double valor)
    {
        this.setId(id);
        this.setNome(nome);
        this.setSaldo(valor);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
    
    
    
}
