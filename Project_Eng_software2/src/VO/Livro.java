/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.sql.Date;
import javax.swing.Icon;

/**
 *
 * @author ronai
 */
public class Livro {
   private int id_livro;
   private String titulo_livro;
   private Date data;
   private double valor;
   private int id_autor;
   private Icon icon;
   private String arquivo;

    public Livro(int id_livro, String titulo_livro, Date data, double valor, int id_autor, Icon icon, String arquivo) {
        this.id_livro = id_livro;
        this.titulo_livro = titulo_livro;
        this.data = data;
        this.valor = valor;
        this.id_autor = id_autor;
        this.icon = icon;
        this.arquivo = arquivo;
    }

    public int getId_livro() {
        return id_livro;
    }

    public void setId_livro(int id_livro) {
        this.id_livro = id_livro;
    }

    public String getTitulo_livro() {
        return titulo_livro;
    }

    public void setTitulo_livro(String titulo_livro) {
        this.titulo_livro = titulo_livro;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getId_autor() {
        return id_autor;
    }

    public void setId_autor(int id_autor) {
        this.id_autor = id_autor;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }
}