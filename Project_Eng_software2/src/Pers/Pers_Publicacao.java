/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pers;

import VO.Usuario;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author riochens
 */
public class Pers_Publicacao {

    public Pers_Publicacao() {
    }

    private ResultSet rs = null;
    int aux;

    public boolean verifica_livro(int id_livro) {
        try (Connection c = Conexao.getConnection();
                PreparedStatement prs = c.prepareStatement("SELECT *FROM Estante where id_Livro = ?;");) {
            prs.setInt(1, id_livro);

            ResultSet valida = prs.executeQuery();

            if (valida.next()) {
                return true;
            }
            return false;

        } catch (SQLException ex) {
            Logger.getLogger(Pers.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void remover(int id_livro) {
        try (Connection c = Conexao.getConnection();
                PreparedStatement prs = c.prepareStatement("delete from Livro where id_livro = ?;");) {
            prs.setInt(1, id_livro);
            prs.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void novo_Cadastro(FileInputStream fis, int aux, String titulo, double valor, Usuario u, String arquivo) {
        try {
            Connection c = Conexao.getConnection();
            PreparedStatement prs = c.prepareStatement("insert into Livro (Titulo, Data, Valor, Autor, Capa, Arquivo) values (?, ?, ?, ?, ?, ?)");
            {
                Date hoje = new Date(System.currentTimeMillis());

                //Date data = new Date(aux);
                prs.setString(1, titulo);
                prs.setDate(2, hoje);
                prs.setDouble(3, valor);
                prs.setInt(4, u.getId());
                prs.setBinaryStream(5, fis, aux);
                prs.setString(6, arquivo);

                prs.executeUpdate();

            }
        } catch (SQLException ex) {

            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
