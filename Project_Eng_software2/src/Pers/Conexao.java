/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pers;

import Abstract.Conec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Rodrigo
 */
public class Conexao extends Conec
  {

    public static Connection conexao = null;

    public Conexao()
      {
      }

    protected static Connection getConnection() throws SQLException
      {
        if (conexao == null)
        {
            conexao = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("nova instancia");
            return conexao;
        }
        System.out.println("singleton");
        return conexao;
      }

    public static Statement Stm() throws SQLException
      {
        Statement stm = conexao.createStatement();
        return stm;
      }

  }
