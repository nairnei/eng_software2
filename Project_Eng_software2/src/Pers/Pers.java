package Pers;

import VO.Livro;
import VO.Usuario;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/*
 *
 * @author rodrigo
 */
public class Pers
  {

    int aux;
    public Connection c;
    private ResultSet rs = null;
    public PreparedStatement prs;
    //foi pra pers_publicq
    private final String SQL_POPULAR_ESTANTE = "SELECT *FROM Livro natural join Estante where id_Usuario = ?;";

    private final String SQL_POPULAR_PUBLICACOES = "select  distinct *from Livro where Autor = ?";
    //foi pra pers_public
    private final String SQL_POPULAR_LIVROS = "select *from Livro";

    private final String SQL_ADICIONAR_USUARIO = "insert into Usuario(Nome, Saldo, Senha) values (?, 0, ?);";

    private final String SQL_ADICIONAR_LIVRO = "insert into livro (Titulo, Data, Valor, Autor, Capa, Arquivo) values (?, ?, ?, ?, ? ,?)";

    private final String SQL_COMPRAR_LIVRO = "insert into Estante(id_Usuario, id_Livro, data_Compra) values (?, ?, ?)";

    private final String SQL_PUBLICAR_LIVRO = "insert into Publicacao(id_Usuario, id_Livro, data_Compra) values (?, ?, ?)";

    private final String SQL_SELECT = "select * from livro ";

    private final String SQL_INSERIR_IMG = "insert into Livro (id, nome, capa) values (?, ?, ?)";

    private final String SQL_SEARCH = "select * from L where titulo = ?;";

    private final String SQL_ENVIAR_COMENTARIO = "insert into Avaliacao(id_Avaliacao ,Livro_id_Livro, Comentario) values( ?, ?, ?);";

    private final String SQL_ALTERAR_SALDO = "UPDATE Usuario SET Saldo = Saldo + ? WHERE id_Usuario = ?;";

    private final String SQL_NOVO_SALDO = "UPDATE Usuario SET Saldo = ? WHERE id_Usuario = ?;";

    private final String SQL_MOSTRAR_COMENTARIO = "SELECT * from Avaliacao WHERE Livro_id_Livro ?;";

    public Pers()
      {
        try
        {
            this.c = Conexao.getConnection();
        } catch (SQLException ex)
        {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
      }

    public void popularTabelaEstante_E(ArrayList<Livro> estante, Usuario u) throws SQLException
      {
        prs = c.prepareStatement(SQL_POPULAR_ESTANTE);
        prs.setInt(1, u.getId());
        rs = prs.executeQuery();
        while (rs.next())
        {
            Livro book;
            Blob image = rs.getBlob("capa");
            byte[] imgByte = image.getBytes(1, (int) image.length());
            ImageIcon ico = new ImageIcon(imgByte);
            book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
            estante.add(book);
        }
      }

    //foi para pers_publiq
    public void popularTabelaEstante_C(ArrayList<Livro> estante, Usuario u) throws SQLException
      {
        prs = c.prepareStatement(SQL_POPULAR_LIVROS);
        rs = prs.executeQuery();
        while (rs.next())
        {
            Livro book;
            Blob image = rs.getBlob("capa");
            byte[] imgByte = image.getBytes(1, (int) image.length());
            ImageIcon ico = new ImageIcon(imgByte);
            book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
            estante.add(book);
        }
      }

    //foi pra pers_public
    public void popularTabelaEstante_P(ArrayList<Livro> estante, Usuario u) throws SQLException
      {
        prs = c.prepareStatement(SQL_POPULAR_PUBLICACOES);
        prs.setInt(1, u.getId());
        rs = prs.executeQuery();
        while (rs.next())
        {
            Livro book;
            Blob image = rs.getBlob("capa");
            byte[] imgByte = image.getBytes(1, (int) image.length());
            ImageIcon ico = new ImageIcon(imgByte);
            book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
            estante.add(book);
        }
      }

    public void popularTabelaEstanteComFiltro(ArrayList<Livro> filtro, String text, Usuario u) throws SQLException
      {
        prs = c.prepareStatement("SELECT *FROM Livro natural join Estante where Titulo Like ? and id_Usuario = ?;");
        prs.setString(1, "%" + text + "%");
        prs.setInt(2, u.getId());
        rs = prs.executeQuery();
        while (rs.next())
        {
            Livro book;
            Blob image = rs.getBlob("capa");
            byte[] imgByte = image.getBytes(1, (int) image.length());
            ImageIcon ico = new ImageIcon(imgByte);
            book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
            filtro.add(book);
        }
      }

    public void popularTabelaPublicacao(ArrayList<Livro> filtro, String text, Usuario u) throws SQLException
      {
        prs = c.prepareStatement("SELECT distinct* FROM Livro where Titulo Like ? and autor = ?");
        prs.setString(1, "%" + text + "%");
        prs.setInt(2, u.getId());
        rs = prs.executeQuery();
        while (rs.next())
        {
            Livro book;
            Blob image = rs.getBlob("capa");
            byte[] imgByte = image.getBytes(1, (int) image.length());
            ImageIcon ico = new ImageIcon(imgByte);
            book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
            filtro.add(book);
        }
      }

    public void popularTabelaCompraComFiltro(ArrayList<Livro> filtro, String text, Usuario u) throws SQLException
      {
        prs = c.prepareStatement("SELECT distinct* FROM Livro natural join  Estante where Titulo Like ?;");
        prs.setString(1, "%" + text + "%");
        rs = prs.executeQuery();
        while (rs.next())
        {
            Livro book;
            Blob image = rs.getBlob("capa");
            byte[] imgByte = image.getBytes(1, (int) image.length());
            ImageIcon ico = new ImageIcon(imgByte);
            book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
            filtro.add(book);
        }
      }

    public void inserirImagem_Stand(JLabel capa1, JLabel capa2, JLabel capa3, JLabel capa4, Usuario u) throws SQLException
      {
        Blob image = null;
        byte[] imgByte = null;
        int i = 0;
        prs = c.prepareStatement(SQL_POPULAR_ESTANTE);
        prs.setInt(1, u.getId());
        rs = prs.executeQuery();
        while (rs.next())
        {
            image = (Blob) rs.getBlob("capa");
            imgByte = image.getBytes(1, (int) image.length());
            ImageIcon icon = new ImageIcon(imgByte);
            if (i == 0)
            {
                capa1.setIcon(icon);
            } else if (i == 1)
            {
                capa2.setIcon(icon);
            } else if (i == 2)
            {
                capa3.setIcon(icon);
            } else if (i == 3)
            {
                capa4.setIcon(icon);
            } else
            {
                break;
            }
            i++;
        }
      }

    public void Ver_Capa(int index, JLabel capa) throws SQLException
      {
        Blob image = null;
        byte[] imgByte = null;
        int i = 0;
        prs = c.prepareStatement("SELECT capa FROM Livro  where id_Livro = ?");
        prs.setInt(1, index);
        rs = prs.executeQuery();
        while (rs.next())
        {
            image = (Blob) rs.getBlob("capa");
            imgByte = image.getBytes(1, (int) image.length());
            ImageIcon icon = new ImageIcon(imgByte);
            capa.setIcon(icon);
        }
      }

    //foi pra pers
    public void novo_Cadastro(FileInputStream fis, int aux, String titulo, double valor, Usuario u, String arquivo)
      {
      }

    //sobrecarga
    public boolean Logar(Usuario login, int senha) throws SQLException
      {
        prs = c.prepareStatement("SELECT *FROM Usuario where nome = ? and senha = ?");
        prs.setString(1, login.getNome());
        prs.setInt(2, senha);
        ResultSet valida = prs.executeQuery();
        if (valida.next())
        {
            login.setSaldo(valida.getDouble("Saldo"));
            login.setId(valida.getInt("id_Usuario"));
            return true;
        }
        return false;
      }

    //sobrecarga
    public boolean Logar(String user) throws SQLException
      {
        prs = c.prepareStatement("SELECT *FROM Usuario where nome = ?;");
        prs.setString(1, user);
        ResultSet valida = prs.executeQuery();
        if (valida.next())
        {
            return true;
        }
        return false;
      }

    public void cadastrar(String text, int parseInt) throws SQLException
      {
        prs = c.prepareStatement(SQL_ADICIONAR_USUARIO);
        prs.setString(1, text);
        prs.setInt(2, parseInt);
        prs.executeUpdate();
      }

    public void search(String text) throws SQLException
      {
        prs = c.prepareStatement(SQL_SEARCH);
        prs.setString(1, text);
        prs.executeQuery();
      }

    public void comprar(int id, double valor, Usuario u) throws SQLException
      {
        prs = c.prepareStatement(SQL_NOVO_SALDO);
        prs.setDouble(1, (u.getSaldo() - valor));
        prs.setInt(2, u.getId());
        prs.executeUpdate();
      }

    public void enviar_comentario(int aux, String text, Usuario u) throws SQLException
      {
        prs = c.prepareStatement(SQL_ENVIAR_COMENTARIO);
        prs.setInt(1, u.getId());
        prs.setInt(2, aux);
        prs.setString(3, text);
        prs.executeUpdate();
      }

    public void alterarSaldo(int id, Double saldo) throws SQLException
      {
        prs = c.prepareStatement(SQL_ALTERAR_SALDO);
        prs.setDouble(1, saldo);
        prs.setInt(2, id);
        prs.executeUpdate();
      }

    public void addNaEstante(int id, Usuario u) throws SQLException
      {
        prs = c.prepareStatement(SQL_COMPRAR_LIVRO);
        prs.setInt(1, u.getId());
        prs.setInt(2, id);
        Date hoje = new Date(System.currentTimeMillis());
        prs.setDate(3, hoje);
        prs.executeUpdate();
      }

    public void mostrarComentario(int aux) throws SQLException
      {
        prs = c.prepareStatement(SQL_MOSTRAR_COMENTARIO);
        {
            prs.setInt(1, aux);
        }
      }

    public void Depositar(int id, double valor) throws SQLException
      {
        prs = c.prepareStatement(SQL_ALTERAR_SALDO);
        prs.setDouble(1, valor);
        prs.setInt(2, id);
        prs.executeUpdate();
      }

    //foi para pers_prublic - R
    public boolean verifica_livro(int id_livro) throws SQLException
      {
        prs = c.prepareStatement("SELECT *FROM Estante where id_Livro = ?;");
        prs.setInt(1, id_livro);
        ResultSet valida = prs.executeQuery();
        return valida.next();
      }

    public void remover(int id_livro) throws SQLException
      {
        prs = c.prepareStatement("delete from livro where id_livro = ?;");
        prs.setInt(1, id_livro);
        prs.executeUpdate();
      }

//sobrecarga
    public void Logar(int id, Usuario u, int id_livro) throws SQLException
      {
        prs = c.prepareStatement("UPDATE estate SET  id_Usuario = ? WHERE id_Usuario = ? and id_Livro = ?;");
        prs.setInt(1, id);
        prs.setInt(2, u.getId());
        prs.setInt(2, id_livro);
        prs.executeUpdate();
      }

    public void ler(int i) throws SQLException, IOException
      {
        prs = c.prepareStatement("SELECT *FROM Livro where id_Livro = ?;");
        prs.setInt(1, i);
        ResultSet valida = prs.executeQuery();
        if (valida.next())
        {
            File meuArquivo = new File(valida.getString("Arquivo"));
            Desktop.getDesktop().open(meuArquivo);
        }
      }

//sobrecarga
    public void comentarios(JTextArea jTextAreaComentario1, JTextArea jTextAreaComentario2, JTextArea jTextAreaComentario3, int aux) throws SQLException
      {
        int i = 0;
        prs = c.prepareStatement("SELECT *FROM Avaliacao where Livro_id_Livro = ?;");
        prs.setInt(1, aux);
        ResultSet valida = prs.executeQuery();
        while (valida.next())
        {
            if (i == 0)
            {
                jTextAreaComentario1.setText(valida.getString("Comentario"));
            } else if (i == 1)
            {
                jTextAreaComentario2.setText(valida.getString("Comentario"));
            } else if (i == 2)
            {
                jTextAreaComentario3.setText(valida.getString("Comentario"));
            } else
            {
                break;
            }
            i++;
        }
      }

    //sobrecarga
    public void comentarios(JTextField comentario, int aux, Usuario u) throws SQLException
      {
        prs = c.prepareStatement("SELECT *FROM Avaliacao where Livro_id_Livro = ? and id_Avaliacao = ?;");
        prs.setInt(1, aux);
        prs.setInt(2, u.getId());
        ResultSet valida = prs.executeQuery();
        while (valida.next())
        {
            comentario.setText(valida.getString("Comentario"));
        }
      }

    public void alterarSenha(JPasswordField jPasswordField1, int senha2, Usuario u) throws SQLException
      {
        prs = c.prepareStatement("UPDATE Usuario SET Senha = ? WHERE Senha = ? and id_Usuario = ?;");
        prs.setInt(1, senha2);
        prs.setInt(2, Integer.parseInt(jPasswordField1.getText()));
        prs.setInt(3, u.getId());
        prs.executeUpdate();
        JOptionPane.showMessageDialog(null, "Senha alterada com sucesso!");
      }

    public void popularTabelaEstante_U(ArrayList<Usuario> u) throws SQLException
      {
        prs = c.prepareStatement("select *from Usuario");
        rs = prs.executeQuery();
        while (rs.next())
        {
            Usuario user = new Usuario(rs.getInt("id_Usuario"), rs.getString("Nome"), rs.getDouble("Saldo"));
            u.add(user);
        }
      }
  }
