/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pers;

import VO.Livro;
import VO.Usuario;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author rodrigo
 */
public class Pers_Consulta {

    private ResultSet rs = null;
    int aux;

    public Pers_Consulta() {
    }

    private final String SQL_POPULAR_ESTANTE = "SELECT *FROM Livro natural join Estante where id_Usuario = ?;";

    private final String SQL_POPULAR_LIVROS = "select *from Livro";

    private final String SQL_POPULAR_PUBLICACOES = "select  distinct *from Livro where Autor = ?";

    public void inserirImagem_Stand(JLabel capa1, JLabel capa2, JLabel capa3, JLabel capa4, Usuario u) {

        Blob image = null;
        byte[] imgByte = null;
        int i = 0;

        try {
            Connection c = Conexao.getConnection();

            PreparedStatement prs = c.prepareStatement(SQL_POPULAR_ESTANTE);
            {
                prs.setInt(1, u.getId());
                rs = prs.executeQuery();

                while (rs.next()) {

                    image = (Blob) rs.getBlob("capa");
                    imgByte = image.getBytes(1, (int) image.length());
                    ImageIcon icon = new ImageIcon(imgByte);

                    if (i == 0) {
                        capa1.setIcon(icon);
                    } else if (i == 1) {
                        capa2.setIcon(icon);
                    } else if (i == 2) {
                        capa3.setIcon(icon);
                    } else if (i == 3) {
                        capa4.setIcon(icon);
                    } else {
                        break;
                    }
                    i++;

                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //-----------
    public void popularTabelaPublicacao(ArrayList<Livro> filtro, String text, Usuario u) {

        int i = 0;
        try {
            Connection c = Conexao.getConnection();
            PreparedStatement prs = c.prepareStatement("SELECT distinct* FROM Livro where Titulo Like ? and autor = ?");
            {
                prs.setString(1, "%" + text + "%");
                prs.setInt(2, u.getId());
                rs = prs.executeQuery();

                while (rs.next()) {
                    Livro book;
                    Blob image = rs.getBlob("capa");
                    byte[] imgByte = image.getBytes(1, (int) image.length());
                    ImageIcon ico = new ImageIcon(imgByte);
                    book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
                    filtro.add(book);

                    i++;
                    System.out.println(i);

                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void popularTabelaEstante_E(ArrayList<Livro> estante, Usuario u) {

        int i = 0;
        try {
            Connection c = Conexao.getConnection();
            PreparedStatement prs = c.prepareStatement(SQL_POPULAR_ESTANTE);
            {
                prs.setInt(1, u.getId());
                rs = prs.executeQuery();

                while (rs.next()) {
                    Livro book;
                    Blob image = rs.getBlob("capa");
                    byte[] imgByte = image.getBytes(1, (int) image.length());
                    ImageIcon ico = new ImageIcon(imgByte);
                    book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
                    estante.add(book);

                    i++;
                    System.out.println(i);

                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void popularTabelaEstante_C(ArrayList<Livro> estante, Usuario u) {

        int i = 0;
        try {
            Connection c = Conexao.getConnection();
            PreparedStatement prs = c.prepareStatement(SQL_POPULAR_LIVROS);
            {

                rs = prs.executeQuery();

                while (rs.next()) {
                    Livro book;
                    Blob image = rs.getBlob("capa");
                    byte[] imgByte = image.getBytes(1, (int) image.length());
                    ImageIcon ico = new ImageIcon(imgByte);
                    book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
                    estante.add(book);

                    i++;
                    System.out.println(i);

                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void popularTabelaEstante_P(ArrayList<Livro> estante, Usuario u) {

        int i = 0;
        try {
            Connection c = Conexao.getConnection();
            PreparedStatement prs = c.prepareStatement(SQL_POPULAR_PUBLICACOES);
            {
                prs.setInt(1, u.getId());

                rs = prs.executeQuery();

                while (rs.next()) {
                    Livro book;
                    Blob image = rs.getBlob("capa");
                    byte[] imgByte = image.getBytes(1, (int) image.length());
                    ImageIcon ico = new ImageIcon(imgByte);
                    book = new Livro(rs.getInt("id_Livro"), rs.getString("Titulo"), rs.getDate("data"), rs.getDouble("valor"), rs.getInt("autor"), ico, rs.getString("arquivo"));
                    estante.add(book);

                    i++;
                    System.out.println(i);

                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Pers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
