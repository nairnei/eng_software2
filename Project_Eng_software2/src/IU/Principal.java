/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IU;

import Pers.ManipulaRelatorio;
import Pers.Pers;
import Pers.Pers_Publicacao;
import RN.RN_Estante;
import RN.RN_Publicacao;
import RN.Regra;
import VO.Usuario;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
//import net.sf.jasperreports.engine.JRException;

/**
 *
 * @author ronai
 */
public class Principal extends javax.swing.JFrame
  {

    private BufferedImage auxX;
    public static File file;
    private Usuario u;
    public String arquivo = "";

    public Principal()
      {
        initComponents();
        Regra r = new Regra();
        jButton_Remover_livro_.setEnabled(false);
        jButton_Leia_.setEnabled(false);
      }

    public void setUser(Usuario user)
      {
        try
        {
            Regra r = new Regra();
            this.u = user;
            jLabel_user.setText(String.valueOf(user.getNome()));
            jLabel_Saldo.setText(String.valueOf(user.getSaldo()));
            r.popularTabela_E(Tabela_Estante, u);
            r.popularTabela_P(Tabela_publicacao, u);
            r.popularTabela_C(Tabela_Compra, u);
            r.inserirImagem(capa1, capa2, capa3, capa4, u);
        } catch (SQLException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
      }

    public Usuario getU()
      {
        return u;
      }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabela_Estante = new javax.swing.JTable();
        jTextarea_comentario = new javax.swing.JTextField();
        jButton_busca1_ = new javax.swing.JButton();
        JTextSearch = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        capa4 = new javax.swing.JLabel();
        capa1 = new javax.swing.JLabel();
        capa2 = new javax.swing.JLabel();
        capa3 = new javax.swing.JLabel();
        jButton_Leia_ = new javax.swing.JButton();
        jBEnviar_comentario = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        Tabela_Compra = new javax.swing.JTable();
        jButton_Comprar = new javax.swing.JButton();
        jLabel_Loja = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jButton_busca2_ = new javax.swing.JButton();
        Jtext_Loja_busca = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaComentario3 = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextAreaComentario1 = new javax.swing.JTextArea();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextAreaComentario2 = new javax.swing.JTextArea();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        Tabela_publicacao = new javax.swing.JTable();
        jButton_anexar_ebook = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        Jtext_busca_publiq = new javax.swing.JTextField();
        jButton_busca3_ = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jText_titulo = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jText_valor = new javax.swing.JTextField();
        Botao_Enviar_ = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jButton_selecionar_capa_ = new javax.swing.JButton();
        capa = new javax.swing.JLabel();
        jButton_Remover_livro_ = new javax.swing.JButton();
        jRadioButton_remover = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPasswordField_atual = new javax.swing.JPasswordField();
        jPasswordField_Nova_senha = new javax.swing.JPasswordField();
        jPasswordField_Nova_Senha_Confirmacao = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        jLabel_user = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel_Saldo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de Auto-publicação de e-book");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel1.setText("Sistema de Compra e venda Online");

        jTabbedPane1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Tabela_Estante.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null}
            },
            new String []
            {
                "ID", "nome", "Data", "Valor"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.Integer.class, java.lang.String.class, java.lang.Object.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        Tabela_Estante.setFocusable(false);
        Tabela_Estante.getTableHeader().setReorderingAllowed(false);
        Tabela_Estante.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                Tabela_EstanteMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(Tabela_Estante);
        if (Tabela_Estante.getColumnModel().getColumnCount() > 0)
        {
            Tabela_Estante.getColumnModel().getColumn(0).setResizable(false);
        }

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 640, 120));

        jTextarea_comentario.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jTextarea_comentarioActionPerformed(evt);
            }
        });
        jTextarea_comentario.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jTextarea_comentarioKeyTyped(evt);
            }
        });
        jPanel1.add(jTextarea_comentario, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 490, 640, 90));

        jButton_busca1_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_busca1_ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton_busca1_, new org.netbeans.lib.awtextra.AbsoluteConstraints(321, 18, 40, 30));

        JTextSearch.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                JTextSearchKeyTyped(evt);
            }
        });
        jPanel1.add(JTextSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(73, 18, 230, -1));

        jLabel20.setText("Busca:");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, -1));

        capa4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dark_gray.png"))); // NOI18N
        jPanel1.add(capa4, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 60, -1, -1));

        capa1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gray.jpg"))); // NOI18N
        capa1.setToolTipText("oi");
        jPanel1.add(capa1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        capa2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dark_gray.png"))); // NOI18N
        jPanel1.add(capa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 60, -1, -1));

        capa3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gray.jpg"))); // NOI18N
        jPanel1.add(capa3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 60, -1, -1));

        jButton_Leia_.setText("Leia Agora");
        jButton_Leia_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_Leia_ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton_Leia_, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 430, -1, -1));

        jBEnviar_comentario.setText("Enviar comentário");
        jBEnviar_comentario.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jBEnviar_comentarioActionPerformed(evt);
            }
        });
        jPanel1.add(jBEnviar_comentario, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 590, -1, -1));

        jLabel4.setText("Selecione um Livro de sua estante e escreva um comentario abaixo:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, -1, -1));

        jTabbedPane1.addTab("Estante", jPanel1);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Tabela_Compra.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, "Africa está em nós", "5 stars", "16/05/2016", "R$ 30.00"}
            },
            new String []
            {
                "ID", "Nome", "Autor", "Publicação", "Valor"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        Tabela_Compra.getTableHeader().setReorderingAllowed(false);
        Tabela_Compra.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                Tabela_CompraMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
                Tabela_CompraMouseEntered(evt);
            }
        });
        jScrollPane2.setViewportView(Tabela_Compra);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 47, 400, 196));

        jButton_Comprar.setText("Comprar");
        jButton_Comprar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_ComprarActionPerformed(evt);
            }
        });
        jPanel2.add(jButton_Comprar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 641, 59));

        jLabel_Loja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dark_gray.png"))); // NOI18N
        jPanel2.add(jLabel_Loja, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 50, -1, -1));

        jLabel17.setText("Comentarios:");
        jPanel2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, -1, -1));

        jButton_busca2_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_busca2_ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton_busca2_, new org.netbeans.lib.awtextra.AbsoluteConstraints(321, 18, 40, 30));

        Jtext_Loja_busca.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                Jtext_Loja_buscaKeyTyped(evt);
            }
        });
        jPanel2.add(Jtext_Loja_busca, new org.netbeans.lib.awtextra.AbsoluteConstraints(73, 18, 230, -1));

        jLabel18.setText("Busca:");
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, -1));

        jTextAreaComentario3.setEditable(false);
        jTextAreaComentario3.setColumns(20);
        jTextAreaComentario3.setRows(5);
        jScrollPane3.setViewportView(jTextAreaComentario3);

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 420, 640, 40));

        jTextAreaComentario1.setEditable(false);
        jTextAreaComentario1.setColumns(20);
        jTextAreaComentario1.setRows(5);
        jScrollPane4.setViewportView(jTextAreaComentario1);

        jPanel2.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, 640, 50));

        jTextAreaComentario2.setEditable(false);
        jTextAreaComentario2.setColumns(20);
        jTextAreaComentario2.setRows(5);
        jScrollPane5.setViewportView(jTextAreaComentario2);

        jPanel2.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, 640, 50));

        jTabbedPane1.addTab("Loja", jPanel2);

        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Tabela_publicacao.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null}
            },
            new String []
            {
                "ID", "Titulo", "Valor"
            }
        )
        {
            boolean[] canEdit = new boolean []
            {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        Tabela_publicacao.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(Tabela_publicacao);

        jPanel9.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 400, 210));

        jButton_anexar_ebook.setText("Anexar E-book");
        jButton_anexar_ebook.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_anexar_ebookActionPerformed(evt);
            }
        });
        jPanel9.add(jButton_anexar_ebook, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 520, 200, 59));

        jLabel8.setText("Busca:");
        jPanel9.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 21, -1, -1));

        Jtext_busca_publiq.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                Jtext_busca_publiqKeyTyped(evt);
            }
        });
        jPanel9.add(Jtext_busca_publiq, new org.netbeans.lib.awtextra.AbsoluteConstraints(73, 18, 230, -1));

        jButton_busca3_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_busca3_ActionPerformed(evt);
            }
        });
        jPanel9.add(jButton_busca3_, new org.netbeans.lib.awtextra.AbsoluteConstraints(321, 18, 40, 30));

        jLabel10.setText("Titulo:");
        jPanel9.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 63, -1, -1));

        jText_titulo.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jText_tituloKeyTyped(evt);
            }
        });
        jPanel9.add(jText_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(478, 60, 104, -1));

        jLabel12.setText("Valor:");
        jPanel9.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 115, -1, -1));

        jText_valor.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jText_valorKeyTyped(evt);
            }
        });
        jPanel9.add(jText_valor, new org.netbeans.lib.awtextra.AbsoluteConstraints(478, 112, 104, -1));

        Botao_Enviar_.setText("Enviar");
        Botao_Enviar_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                Botao_Enviar_ActionPerformed(evt);
            }
        });
        jPanel9.add(Botao_Enviar_, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 520, 420, 59));

        jLabel19.setText("Anexar Capa:");
        jPanel9.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, -1, -1));

        jButton_selecionar_capa_.setText("Select");
        jButton_selecionar_capa_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_selecionar_capa_ActionPerformed(evt);
            }
        });
        jPanel9.add(jButton_selecionar_capa_, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 280, -1, -1));

        capa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dark_gray.png"))); // NOI18N
        jPanel9.add(capa, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 310, -1, -1));

        jButton_Remover_livro_.setText("Remover e-book");
        jButton_Remover_livro_.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_Remover_livro_ActionPerformed(evt);
            }
        });
        jPanel9.add(jButton_Remover_livro_, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 250, -1, -1));

        jRadioButton_remover.setText("Quero remover um e-book publicado");
        jRadioButton_remover.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButton_removerActionPerformed(evt);
            }
        });
        jPanel9.add(jRadioButton_remover, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 220, -1, -1));

        jTabbedPane1.addTab("Publique", jPanel9);

        jLabel6.setText("Nova Senha:");

        jButton1.setText("Alterar Senha");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Minha Estante");
        jButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Publicacões");
        jButton3.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Livros Disponiveis");
        jButton4.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel11.setText("Relatorios");

        jLabel13.setText("Senha Atual:");

        jLabel14.setText("Confirmar Senha:");

        jPasswordField_atual.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jPasswordField_atualKeyTyped(evt);
            }
        });

        jPasswordField_Nova_senha.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jPasswordField_Nova_senhaKeyTyped(evt);
            }
        });

        jPasswordField_Nova_Senha_Confirmacao.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jPasswordField_Nova_Senha_ConfirmacaoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addComponent(jButton2)
                        .addGap(50, 50, 50)
                        .addComponent(jButton3)
                        .addGap(39, 39, 39)
                        .addComponent(jButton4))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel13))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton1)
                                    .addComponent(jPasswordField_Nova_senha)
                                    .addComponent(jPasswordField_Nova_Senha_Confirmacao)
                                    .addComponent(jPasswordField_atual)))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jPasswordField_atual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jPasswordField_Nova_senha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jPasswordField_Nova_Senha_Confirmacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addContainerGap(401, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Configurações", jPanel3);

        jLabel2.setText("Bem vindo");

        jLabel_user.setText("user");

        jLabel3.setText("Saldo:");

        jLabel_Saldo.setText("Saldo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(254, 254, 254)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel_user)
                        .addGap(60, 60, 60)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel_Saldo)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTabbedPane1)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel_user)
                            .addComponent(jLabel3)
                            .addComponent(jLabel_Saldo))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 651, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private void Botao_Enviar_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botao_Enviar_ActionPerformed
        String Titulo = jText_titulo.getText();
        String Valor = jText_valor.getText();
        if (Titulo.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "Insira Titulo!");
        } else if (Valor.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "Insira Valor!");
        } else if (file == null)
        {
            JOptionPane.showMessageDialog(null, "Capa nao selecionada!");
        } else if (arquivo.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "e-book nao selecionado!");
        } else if (JOptionPane.showConfirmDialog(null, "Você só poderá remover um livro publicado caso ninguém o tenha adquirido.\n"
                + "Tem certeza que deseja publicá-lo?") == 0)
        {
            Pers_Publicacao p = new Pers_Publicacao();
            int aux = 0;
            try
            {
                aux = (int) file.length();
                FileInputStream fis = new FileInputStream(file);
                if (aux > 65535)
                {
                    JOptionPane.showMessageDialog(rootPane, "Tamanho máximo excedido");
                    return;
                }
                //p.novo_Cadastro_Livro(fis, aux);
                p.novo_Cadastro(fis, aux, jText_titulo.getText(), Double.parseDouble(jText_valor.getText()), u, arquivo);
            } catch (FileNotFoundException ex)
            {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        RN_Publicacao r = new RN_Publicacao();
        r.popularTabela_E(Tabela_Estante, u);
        r.popularTabela_P(Tabela_publicacao, u);
        r.popularTabela_C(Tabela_Compra, u);
        r.inserirImagem(capa1, capa2, capa3, capa4, u);
    }//GEN-LAST:event_Botao_Enviar_ActionPerformed

    private void jButton_Leia_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Leia_ActionPerformed
        try
        {
            Regra r = new Regra();
            int id_livro = (int) Tabela_Estante.getValueAt(Tabela_Estante.getSelectedRow(), 0);
            r.ler(id_livro);
        } catch (SQLException | IOException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton_Leia_ActionPerformed

    private void jButton_selecionar_capa_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_selecionar_capa_ActionPerformed
        JFileChooser arquivo = new JFileChooser();
        String img;
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Imagens", "gif", "jpg", "png", "jpeg", "bitmap");
        arquivo.setFileFilter(filtro);
        arquivo.setAcceptAllFileFilterUsed(false);
        arquivo.setFileSelectionMode(JFileChooser.FILES_ONLY);
        file = new File("user.dir");
        int salvar = arquivo.showOpenDialog(null);
        if (salvar == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                BufferedImage imagem = ImageIO.read(arquivo.getSelectedFile());
                ImageIcon aux = new ImageIcon(imagem);
                aux.setImage(aux.getImage().getScaledInstance(150, 200, 100));
                capa.setIcon(aux);
                file = arquivo.getSelectedFile();
            } catch (IOException ex)
            {
            }
        }
    }//GEN-LAST:event_jButton_selecionar_capa_ActionPerformed

    private void jButton_busca1_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_busca1_ActionPerformed
        try
        {
            Regra r = new Regra();
            r.Busca_Estante(Tabela_Estante, JTextSearch.getText(), u);
        } catch (SQLException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton_busca1_ActionPerformed

    private void jButton_busca2_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_busca2_ActionPerformed
        try
        {
            Regra r = new Regra();
            r.Busca_Compra(Tabela_Compra, Jtext_Loja_busca.getText(), u);
        } catch (SQLException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton_busca2_ActionPerformed

    private void jButton_ComprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ComprarActionPerformed
        Regra r = new Regra();
        int id_livro = (int) Tabela_Compra.getValueAt(Tabela_Compra.getSelectedRow(), 0);
        int id_autor = (int) Tabela_Compra.getValueAt(Tabela_Compra.getSelectedRow(), 2);
        Double valor = (double) Tabela_Compra.getValueAt(Tabela_Compra.getSelectedRow(), 4);
        if (u.getSaldo() >= valor)
        {
            try
            {
                r.comprar(id_livro, valor, u);
                r.depositar(id_autor, valor);
                if (id_autor != u.getId())
                {
                    u.setSaldo(u.getSaldo() - valor);
                    jLabel_Saldo.setText(u.getSaldo().toString());
                }
                r.popularTabela_E(Tabela_Estante, u);
                r.popularTabela_P(Tabela_publicacao, u);
                r.popularTabela_C(Tabela_Compra, u);
                r.inserirImagem(capa1, capa2, capa3, capa4, u);
            } catch (SQLException ex)
            {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else
        {
            JOptionPane.showMessageDialog(null, "saldo insuficiente");
        }
    }//GEN-LAST:event_jButton_ComprarActionPerformed

    private void JTextSearchKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTextSearchKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarEspecial(evt);
    }//GEN-LAST:event_JTextSearchKeyTyped

    private void Jtext_Loja_buscaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jtext_Loja_buscaKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarEspecial(evt);
    }//GEN-LAST:event_Jtext_Loja_buscaKeyTyped

    private void Jtext_busca_publiqKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jtext_busca_publiqKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarEspecial(evt);
    }//GEN-LAST:event_Jtext_busca_publiqKeyTyped

    private void jText_tituloKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jText_tituloKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarMuitoEspecial(evt);
    }//GEN-LAST:event_jText_tituloKeyTyped

    private void jText_valorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jText_valorKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarInt(evt);
    }//GEN-LAST:event_jText_valorKeyTyped

    private void jTextarea_comentarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextarea_comentarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextarea_comentarioActionPerformed

    private void jTextarea_comentarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextarea_comentarioKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarMuitoEspecial(evt);
    }//GEN-LAST:event_jTextarea_comentarioKeyTyped

    private void jBEnviar_comentarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBEnviar_comentarioActionPerformed
        try
        {
            // TODO add your handling code here:
            RN_Estante r = new RN_Estante();
            r.enviar_comentario((int) Tabela_Estante.getValueAt(Tabela_Estante.getSelectedRow(), 0), jTextarea_comentario.getText(), u);
            jBEnviar_comentario.setVisible(false);
        } catch (SQLException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jBEnviar_comentarioActionPerformed

    private void jButton_busca3_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_busca3_ActionPerformed
        // TODO add your handling code here:
        RN_Publicacao r = new RN_Publicacao();
        r.Busca_Publicacao(Tabela_publicacao, Jtext_busca_publiq.getText(), u);
    }//GEN-LAST:event_jButton_busca3_ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String senha = jPasswordField_atual.getText();
        String senha1 = jPasswordField_Nova_senha.getText();
        String senha2 = jPasswordField_Nova_Senha_Confirmacao.getText();
        if (senha1.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "insira a nova senha");
        } else if (senha.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "insira sua senha atual");
        } else if (senha2.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "confirme sua senha");
        } else if (senha1.compareTo(senha2) == 0)
        {
            try
            {
                System.out.println("Senhas iguais");
                Regra r = new Regra();
                r.alterarSenha(jPasswordField_atual, Integer.parseInt(senha2), u);
            } catch (SQLException ex)
            {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else
        {
            System.out.println("Senhas diferentes");
            JOptionPane.showMessageDialog(null, "Senhas diferentes");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void Tabela_CompraMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_Tabela_CompraMouseClicked
    {//GEN-HEADEREND:event_Tabela_CompraMouseClicked
        try
        {
            Pers p = new Pers();
            Regra r = new Regra();
            int aux = (int) Tabela_Compra.getValueAt(Tabela_Compra.getSelectedRow(), 0);
            p.Ver_Capa(aux, jLabel_Loja);
            jTextAreaComentario1.setText("");
            jTextAreaComentario2.setText("");
            jTextAreaComentario3.setText("");
            p.comentarios(jTextAreaComentario1, jTextAreaComentario2, jTextAreaComentario3, aux);
        } catch (SQLException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Tabela_CompraMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton2ActionPerformed
    {//GEN-HEADEREND:event_jButton2ActionPerformed
        Regra r = new Regra();
        ManipulaRelatorio mr = new ManipulaRelatorio();
        try
        {
//            mr.gerar_relatorio_estante(u.getId());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jPasswordField_atualKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jPasswordField_atualKeyTyped
    {//GEN-HEADEREND:event_jPasswordField_atualKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarInt(evt);
    }//GEN-LAST:event_jPasswordField_atualKeyTyped

    private void jPasswordField_Nova_senhaKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jPasswordField_Nova_senhaKeyTyped
    {//GEN-HEADEREND:event_jPasswordField_Nova_senhaKeyTyped
        Regra r = new Regra();
        r.validarInt(evt);
    }//GEN-LAST:event_jPasswordField_Nova_senhaKeyTyped

    private void jPasswordField_Nova_Senha_ConfirmacaoKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jPasswordField_Nova_Senha_ConfirmacaoKeyTyped
    {//GEN-HEADEREND:event_jPasswordField_Nova_Senha_ConfirmacaoKeyTyped
        Regra r = new Regra();
        r.validarInt(evt);
    }//GEN-LAST:event_jPasswordField_Nova_Senha_ConfirmacaoKeyTyped

    private void jButton_Remover_livro_ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton_Remover_livro_ActionPerformed
    {//GEN-HEADEREND:event_jButton_Remover_livro_ActionPerformed
        RN_Publicacao r = new RN_Publicacao();
        int id_livro = (int) Tabela_publicacao.getValueAt(Tabela_publicacao.getSelectedRow(), 0);
        r.remover_ebook(id_livro);
        r.popularTabela_P(Tabela_publicacao, u);
        r.popularTabela_C(Tabela_Compra, u);
    }//GEN-LAST:event_jButton_Remover_livro_ActionPerformed

    private void jRadioButton_removerActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jRadioButton_removerActionPerformed
    {//GEN-HEADEREND:event_jRadioButton_removerActionPerformed
        if (jRadioButton_remover.isSelected())
        {
            jButton_Remover_livro_.setEnabled(true);
        } else
        {
            jButton_Remover_livro_.setEnabled(false);
        }
    }//GEN-LAST:event_jRadioButton_removerActionPerformed

    private void jButton_anexar_ebookActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton_anexar_ebookActionPerformed
    {//GEN-HEADEREND:event_jButton_anexar_ebookActionPerformed
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("pdf", "pdf");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                arquivo = chooser.getSelectedFile().getCanonicalPath();
            } catch (IOException ex)
            {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton_anexar_ebookActionPerformed

    private void Tabela_EstanteMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_Tabela_EstanteMouseClicked
    {//GEN-HEADEREND:event_Tabela_EstanteMouseClicked
        try
        {
            Pers p = new Pers();
            int aux = (int) Tabela_Estante.getValueAt(Tabela_Estante.getSelectedRow(), 0);
            if (String.valueOf(aux) == "")
            {
                jButton_Leia_.setEnabled(false);
            } else
            {
                jButton_Leia_.setEnabled(true);
            }
            jTextarea_comentario.setText("");
            p.comentarios(jTextarea_comentario, aux, u);
            if (jTextarea_comentario.getText().isEmpty())
            {
                jBEnviar_comentario.setVisible(true);
            } else
            {
                jBEnviar_comentario.setVisible(false);
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Tabela_EstanteMouseClicked

    private void Tabela_CompraMouseEntered(java.awt.event.MouseEvent evt)//GEN-FIRST:event_Tabela_CompraMouseEntered
    {//GEN-HEADEREND:event_Tabela_CompraMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_Tabela_CompraMouseEntered

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton4ActionPerformed
    {//GEN-HEADEREND:event_jButton4ActionPerformed
        /*try {
            ManipulaRelatorio mr = new ManipulaRelatorio();
            mr.gerar_relatorio_livro();
        } catch (JRException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
         */
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton3ActionPerformed
    {//GEN-HEADEREND:event_jButton3ActionPerformed
        /*      try {
            ManipulaRelatorio mr = new ManipulaRelatorio();
            mr.gerar_relatorio_publicacao(u.getId());
        } catch (JRException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
         */
    }//GEN-LAST:event_jButton3ActionPerformed

    public static void main(String args[])
      {
        java.awt.EventQueue.invokeLater(new Runnable()
          {
            public void run()
              {
                new Principal().setVisible(true);
              }
          });
      }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Botao_Enviar_;
    private javax.swing.JTextField JTextSearch;
    private javax.swing.JTextField Jtext_Loja_busca;
    private javax.swing.JTextField Jtext_busca_publiq;
    private javax.swing.JTable Tabela_Compra;
    private javax.swing.JTable Tabela_Estante;
    private javax.swing.JTable Tabela_publicacao;
    private javax.swing.JLabel capa;
    private javax.swing.JLabel capa1;
    private javax.swing.JLabel capa2;
    private javax.swing.JLabel capa3;
    private javax.swing.JLabel capa4;
    private javax.swing.JButton jBEnviar_comentario;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton_Comprar;
    private javax.swing.JButton jButton_Leia_;
    private javax.swing.JButton jButton_Remover_livro_;
    private javax.swing.JButton jButton_anexar_ebook;
    private javax.swing.JButton jButton_busca1_;
    private javax.swing.JButton jButton_busca2_;
    private javax.swing.JButton jButton_busca3_;
    private javax.swing.JButton jButton_selecionar_capa_;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel_Loja;
    private javax.swing.JLabel jLabel_Saldo;
    private javax.swing.JLabel jLabel_user;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPasswordField jPasswordField_Nova_Senha_Confirmacao;
    private javax.swing.JPasswordField jPasswordField_Nova_senha;
    private javax.swing.JPasswordField jPasswordField_atual;
    private javax.swing.JRadioButton jRadioButton_remover;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextAreaComentario1;
    private javax.swing.JTextArea jTextAreaComentario2;
    private javax.swing.JTextArea jTextAreaComentario3;
    private javax.swing.JTextField jText_titulo;
    private javax.swing.JTextField jText_valor;
    private javax.swing.JTextField jTextarea_comentario;
    // End of variables declaration//GEN-END:variables

  }
