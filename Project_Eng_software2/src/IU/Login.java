/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IU;

import RN.Regra;
import VO.Admin;
import VO.Usuario;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author ronai
 */
public class Login extends javax.swing.JFrame
  {

    public Login()
      {
        initComponents();
        ImageIcon mg = new javax.swing.ImageIcon(getClass().getResource("/imagens/config.png"));
        mg.setImage(mg.getImage().getScaledInstance(15, 15, 100));
        BotaoConf.setIcon(mg);
      }

    public void A_login(String text)
      {
        jButton_login.setText(text);
      }

    public void C_login(String text)
      {
        jButton_cadastrar.setText(text);
      }

    public void C_Visible(Boolean aux)
      {
        jButton_cadastrar.setVisible(aux);
      }

    public void A_Visible(Boolean aux)
      {
        jButton_login.setVisible(aux);
      }

    public JButton getjButton_cadastrar()
      {
        return jButton_cadastrar;
      }

    public static void main(String args[])
      {
        java.awt.EventQueue.invokeLater(new Runnable()
          {
            public void run()
              {
                new Login().setVisible(true);
              }
          });
      }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jLabel1 = new javax.swing.JLabel();
        Jtext_login = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButton_login = new javax.swing.JButton();
        jButton_cadastrar = new javax.swing.JButton();
        BotaoConf = new javax.swing.JButton();
        jtext_senha = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Login");

        Jtext_login.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                Jtext_loginActionPerformed(evt);
            }
        });
        Jtext_login.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                Jtext_loginKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                Jtext_loginKeyTyped(evt);
            }
        });

        jLabel2.setText("Senha");

        jButton_login.setText("Login");
        jButton_login.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_loginActionPerformed(evt);
            }
        });

        jButton_cadastrar.setText("Cadastrar");
        jButton_cadastrar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton_cadastrarActionPerformed(evt);
            }
        });

        BotaoConf.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                BotaoConfActionPerformed(evt);
            }
        });

        jtext_senha.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jtext_senhaActionPerformed(evt);
            }
        });
        jtext_senha.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyTyped(java.awt.event.KeyEvent evt)
            {
                jtext_senhaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(BotaoConf, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(Jtext_login, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
                            .addComponent(jtext_senha, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton_login, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton_cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 33, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(Jtext_login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtext_senha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_login)
                    .addComponent(jButton_cadastrar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(BotaoConf)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_loginActionPerformed
        // TODO add your handling code here:
        Principal iu = new Principal();
        Regra r = new Regra();
        String login = Jtext_login.getText();
        String Senha = jtext_senha.getText();
        if (login.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "insira login");
        } else if (Senha.compareTo("") == 0)
        {
            JOptionPane.showMessageDialog(null, "insira senha");
        } else
        {
            Usuario user = new Usuario(Jtext_login.getText());
            try
            {
                r.logar(user, Integer.parseInt(jtext_senha.getText()), iu);
            } catch (SQLException ex)
            {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.dispose();
        }
    }//GEN-LAST:event_jButton_loginActionPerformed

    private void jButton_cadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cadastrarActionPerformed
        // TODO add your handling code here:
        //Cadastro cadastro = new Cadastro();
        Cadastro c = new Cadastro();
        c.setVisible(true);
    }//GEN-LAST:event_jButton_cadastrarActionPerformed

    private void BotaoConfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoConfActionPerformed
        String aux = Jtext_login.getText();
        Admin adm = new Admin();
        if (aux.compareTo(adm.nome) == 0)
        {
            if (Integer.parseInt(jtext_senha.getText()) == adm.senha)
            {
                Admin_conf a = new Admin_conf();
                a.setVisible(true);
            }
        }
    }//GEN-LAST:event_BotaoConfActionPerformed

    private void jtext_senhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtext_senhaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtext_senhaActionPerformed

    private void Jtext_loginKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jtext_loginKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jtext_loginKeyPressed

    private void Jtext_loginKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jtext_loginKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarEspecial(evt);
    }//GEN-LAST:event_Jtext_loginKeyTyped

    private void jtext_senhaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtext_senhaKeyTyped
        // TODO add your handling code here:
        Regra r = new Regra();
        r.validarInt(evt);
    }//GEN-LAST:event_jtext_senhaKeyTyped

    private void Jtext_loginActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_Jtext_loginActionPerformed
    {//GEN-HEADEREND:event_Jtext_loginActionPerformed
        // TODO add your handling code here:
        Regra r = new Regra();
        // r.validarCampoVazio(parsStr Jtext_login.getText()
    }//GEN-LAST:event_Jtext_loginActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotaoConf;
    private javax.swing.JTextField Jtext_login;
    private javax.swing.JButton jButton_cadastrar;
    private javax.swing.JButton jButton_login;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPasswordField jtext_senha;
    // End of variables declaration//GEN-END:variables
  }
