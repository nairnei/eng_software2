-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema e-bd
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema e-bd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `e-bd` DEFAULT CHARACTER SET utf8 ;
USE `e-bd` ;

-- -----------------------------------------------------
-- Table `e-bd`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-bd`.`Usuario` (
  `id_Usuario` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(45) NOT NULL,
  `Saldo` DOUBLE NOT NULL,
  `Senha` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_Usuario`),
  UNIQUE INDEX `id_Usuario_UNIQUE` (`id_Usuario` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e-bd`.`Livro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-bd`.`Livro` (
  `id_Livro` INT NOT NULL AUTO_INCREMENT,
  `Titulo` VARCHAR(45) NOT NULL,
  `Data` DATE NOT NULL,
  `Valor` DOUBLE NOT NULL,
  `Autor` VARCHAR(45) NULL,
  `Capa` BLOB NULL,
  `Arquivo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_Livro`),
  UNIQUE INDEX `id_Livro_UNIQUE` (`id_Livro` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e-bd`.`Estante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-bd`.`Estante` (
  `id_Usuario` INT UNSIGNED NOT NULL,
  `id_Livro` INT NOT NULL,
  `data_Compra` DATE NULL,
  INDEX `fk_Usuario_has_Livro_Livro1_idx` (`id_Livro` ASC),
  INDEX `fk_Usuario_has_Livro_Usuario_idx` (`id_Usuario` ASC),
  CONSTRAINT `fk_Usuario_has_Livro_Usuario`
    FOREIGN KEY (`id_Usuario`)
    REFERENCES `e-bd`.`Usuario` (`id_Usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_has_Livro_Livro1`
    FOREIGN KEY (`id_Livro`)
    REFERENCES `e-bd`.`Livro` (`id_Livro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e-bd`.`Avaliacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-bd`.`Avaliacao` (
  `id_Avaliacao` INT NOT NULL,
  `Comentario` VARCHAR(256) NULL,
  `Nota` INT NULL,
  `Livro_id_Livro` INT NOT NULL,
  INDEX `fk_Avaliacao_Livro1_idx` (`Livro_id_Livro` ASC),
  CONSTRAINT `fk_Avaliacao_Livro1`
    FOREIGN KEY (`Livro_id_Livro`)
    REFERENCES `e-bd`.`Livro` (`id_Livro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e-bd`.`Publicacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-bd`.`Publicacao` (
  `id_Usuario` INT UNSIGNED NOT NULL,
  `id_Livro` INT NOT NULL,
  INDEX `fk_Publicacao_Usuario1_idx` (`id_Usuario` ASC),
  INDEX `fk_Publicacao_Livro1_idx` (`id_Livro` ASC),
  CONSTRAINT `fk_Publicacao_Usuario1`
    FOREIGN KEY (`id_Usuario`)
    REFERENCES `e-bd`.`Usuario` (`id_Usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Publicacao_Livro1`
    FOREIGN KEY (`id_Livro`)
    REFERENCES `e-bd`.`Livro` (`id_Livro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
